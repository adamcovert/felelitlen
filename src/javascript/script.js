document.addEventListener('DOMContentLoaded', function(){
  'use strict';

  svg4everybody();

  var instance = Bricks({
    container: '.container',
    packed: 'data-packed',
    position: false,
    sizes: [{
        columns: 2,
        gutter: 0
    }, {
        mq: "600px",
        columns: 3,
        gutter: 0
    }, {
        mq: "800px",
        columns: 4,
        gutter: 0
    }]
  })

  instance.pack();

  instance
  .on('pack',   () => console.log('ALL grid items packed.'))
  .on('update', () => console.log('NEW grid items packed.'))
  .on('resize', size => console.log('The grid has be re-packed to accommodate a new BREAKPOINT.'))

  instance
    .resize(true)     // bind resize handler
    .pack()           // pack initial items

  modals.init();
});