ready(function () {

  var project = document.querySelector('.project');
  var projectTitle = document.querySelector('.project h3');
  var allProjectTitle = document.querySelectorAll('.project h3');

  document.addEventListener('scroll', function () {

    for ( var i = 0; i < allProjectTitle.length; i++ ) {
      if ( isInViewport(allProjectTitle[i]) ) {
        allProjectTitle[i].style.transform = 'translate(0,' + i++ + '%)';
      }
    }
  });

});